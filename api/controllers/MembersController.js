/**
 * MembersController
 *
 * @description :: Server-side logic for managing members
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    create : function(req,res){
        var name = req.body.name;
        var password = req.body.password;
        var email = req.body.email;
        var number = req.body.number;
        Members.findOne({name:name}).then(function(user){
            if(user){
                req.session.errorMessage = 'Username already exists';
                res.redirect("/members/signup");
                return;
            }else{
                Members.create({name:name, password:password,email:email,number:number}).exec(function(err,member){               
                    if (err){
                        req.session.errorMessage =err;
                        res.redirect("/");
                        return;
                    }  
                    req.session.userId = member.id;
                    req.session.loggedin = true;    
                    res.redirect('/members/hobby');  
                });
            }
           
        })

         
               
       
    },

    signup : function(req,res){
        res.view();
       
    },

    enter : function(req,res){
       res.view();    
    },

    logout: function(req,res){
        req.session.loggedin = false;
        res.redirect("/");
    },

    loggin : function(req,res){
        var name = req.body.name;
        var password = req.body.password;
        Members.findOne({
                name : name,
                password:password
        })
        .then(function(member){
            if(!member){  
                req.session.loginError= 'Wrong Username or Password';  
                res.redirect('/members/enter')
                return;
            }
            req.session.loggedin = true;
            req.session.userId = member.id;    
            res.redirect('/members/hobby')
        });           
    },

    delete : function(req,res){
        Members.destroy({id:req.params.id}).exec(function(err){
            if (err){
                res.send(500,{error: 'Database Error'});
            }

            res.redirect('/members/hobby');
        });
        return false;
    },

    hobby : function(req,res){
        Members.findOne({id:req.session.userId}).populateAll().exec(function(err,members){
            if(err){
                res.send(500,{error: 'Database Error'});
            }
            res.view({
                users:members});

        });
    },

    aboutus : function(req,res){
        res.view();
    }

  
};

